



<p align="center">
 <a href="https://gitlab.com/sotnasaj/ja3pi/" target="blank"><img src="https://gitlab.com/sotnasaj/ja3pi/-/raw/master/repoImages/ja3pi.png" width="175" alt="JA3PI logo" /></a>
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="420" alt="Nest Logo" /></a>
</p>

## Description

Single endpoint news aggregator for [NYTimes API](https://developer.nytimes.com/) and [The Guardian API](https://open-platform.theguardian.com/documentation/)
[Nest](https://github.com/nestjs/nest) 
, built with NestJs

## Running the app

```bash
# development
$ npm start

# watch mode
$ npm run start:dev

```
*The app runs on port 3000*

## Usage

Locally, the entrypoint for searches is:

- http://localhost:3000/ja3pi/search  [GET]

#### Login (kind of optional)

- You can search on NYTimes with no Auth requirements. (see ***source below*** ) 

- For search in The Guardians source (see ***source below*** ) or both API's, you need to 'login' at:

- http://localhost:7000/auth/login  [POST]

using a json body with, e.g  ```{"username": "random"}``` , just to 'simulate' something

``` 
{
	"username": "Rhyiot"
}
```
after that, you will get a response with the JSON web Token. Use this JWT to request searches from Whatever source.

To use the JWT , use it in the Bearer Authentication Token in every request. 

PD: Token's expires every 120s

Then we can use the following parameters to filter:

#### q (Required)

search filter for content.
    
- example: http://localhost:3000/ja3pi/search?q=Trump

#### source (Optional)


To specify the source, it can be either 'guardians' or 'nytimes'.
    
- example: http://localhost:3000/ja3pi/search?q=trump&source=nytimes

#### from_date (Optional)
Return only content published on or after that date, e.g. 2014-02-16,<br>
date needs to be in YY-MM-DD format

- example: http://localhost:3000/ja3pi/search?q=trump&from_date=2014-02-16


#### to_date (Optional)

Return only content published on or before that date, e.g. 2014-02-17,<br>
date needs to be in YY-MM-DD format

- example: http://localhost:3000/ja3pi/search?q=trump&to_date=2014-02-16

#### page (Optional)

To specify the page of the results, from 1 - 200

- example: http://localhost:3000/ja3pi/search?q=trump&page=5


### And a full example


- http://localhost:3000/ja3pi/search?source=nytimes&q=huitzilopochtli&from_date=2016-01-01&to_date=2016-12-31&page=1
