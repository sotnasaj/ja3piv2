import {Length} from 'class-validator';
export class UserDto{
    @Length(1,25)
    username: string
}