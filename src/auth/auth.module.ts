
import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule, JwtModuleOptions } from '@nestjs/jwt';
import { JwtStrategy } from './jwt.strategy';
import { AuthController } from './auth.controller';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
    imports: [
        PassportModule.register({
            defaultStrategy: 'jwt', property: 'user',
            session: false,
        }),
        JwtModule.registerAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService):Promise<JwtModuleOptions> => ({
                secret: configService.get('app.jwtSecret'),
                signOptions: { expiresIn: '120s' },
            }),
            inject:[ConfigService]
        }),
    ],
    providers: [AuthService, JwtStrategy],
    exports: [PassportModule, JwtModule],
    controllers: [AuthController],
})
export class AuthModule { }