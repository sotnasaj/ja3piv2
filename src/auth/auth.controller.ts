import { Controller,Post, Body, UsePipes, ValidationPipe } from '@nestjs/common';
import {AuthService} from './auth.service';
import {UserDto} from './dto/userDTO';

@Controller('auth')
export class AuthController {

    constructor(private readonly authService:AuthService){

    }
    @Post('/login')
    @UsePipes(new ValidationPipe())
    async login(@Body() user: UserDto) {
        const token = this.authService.login(user.username);
        return token;
    }
}
