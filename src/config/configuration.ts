export default () => ({
    app: {
        port: parseInt(process.env.APP_PORT, 10) || 3000,
        jwtSecret: process.env.JWTSECRET || 'papaya',
    },
    nytimes: {
        key: process.env.NYTIMES_KEY,
        endpoint: 'https://api.nytimes.com/svc/search/v2/articlesearch.json',
    },
    guardians: {
        key: process.env.GUARDIANS_KEY,
        endpoint: 'https://content.guardianapis.com/search',
    }
})