import { Injectable, HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
//Services
import { ConfigService } from '@nestjs/config';
//Interfaces and DTO's
import { GuardiansQuery } from './dto/guardiansQuery';
import { GuardiansResponse } from './interfaces/guardiansResponse';



@Injectable()
export class GuardiansService {
    private readonly endpoint: string;

    constructor(
        private readonly configService: ConfigService,
        private readonly httpService: HttpService
    ) {
        this.endpoint = configService.get<string>('guardians.endpoint');
    }

    search(queryData: GuardiansQuery, apiKey: string): Observable<AxiosResponse<GuardiansResponse>> {
        return this.httpService.get<GuardiansResponse>(this.endpoint + queryData.toUri() + `&api-key=${apiKey}`);
    }
}
