export class GuardiansQuery {

    public q: string;
    public beingDate: string | undefined;
    public endDate: string | undefined;
    public page: number | undefined;

    constructor({ q, beingDate, endDate, page }: {
        q: string, beingDate?: string, endDate?: string, page?: number
    }) {
        this.q = q;
        this.beingDate = beingDate;
        this.endDate = endDate;
        this.page = page;
    }

    toUri() {
        const uri: string[] = [];
        uri.push(`?q=${this.q}`);
        if (this.beingDate) uri.push(`&from-date=${this.beingDate}`);
        if (this.endDate) uri.push(`&to-date=${this.endDate}`);
        if(this.page) uri.push(`&page=${this.page}`)
        return uri.join('');
    }
}