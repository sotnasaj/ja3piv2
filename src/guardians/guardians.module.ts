import { Module, HttpModule } from '@nestjs/common';
//Services
import { GuardiansService } from './guardians.service';

@Module({
  imports: [HttpModule],
  providers: [GuardiansService],
  exports: [GuardiansService]
})
export class GuardiansModule {}
