import { IsOptional, IsNotEmpty , IsIn, Min, Max, IsInt, IsISO8601} from 'class-validator';
import {Transform} from 'class-transformer';
export class SearchDTO {

    @IsNotEmpty()
    q: string;

    @IsOptional()
    @IsISO8601()
    from_date: string;

    @IsOptional()
    @IsISO8601()
    to_date: string;

    @IsOptional()
    @IsIn(['nytimes','guardians'])
    source: string;

    @IsOptional()
    @Transform(parseInt)
    @IsInt()
    @Min(1)
    @Max(200)
    page: number;

}