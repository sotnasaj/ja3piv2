export interface SearchResponse{
    ulr: string,
    tittle: string,
    section: string,
    publicationDate: string,
    type: string,
    source?: string 
}
/*
http://localhost:3000/ja3pi/search?source=nytimes&q=trump&from_date=20120101&to_date=20121231



NYTIMES
response:
    docs:
        0:
            web_url
            abstract
            section_name
            pub_date
            document_type

https://api.nytimes.com/svc/search/v2/articlesearch.json?
q=trump
&fq=source:(%22The%20New%20York%20Times%22)%20AND%20news_desk:(%22Sports%22)
&begin_date=20120101
&end_date=20121231
&fl=web_url,abstract
&api-key=omJl5ITI2wXowQqA7KYf810Drnw5P2Ki
*/