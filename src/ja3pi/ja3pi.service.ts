import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Observable, zip } from 'rxjs';
import { map } from 'rxjs/operators';
//Interfaces and DTO
import { GuardiansQuery } from '../guardians/dto/guardiansQuery';
import { NYTimesQuery } from '../nytimes/dto/nytimesQuery';
import { SearchDTO } from './dto/SearchDTO';
import { SearchResponse } from './interfaces/searchResponse';
//Services
import { NYTimesService } from 'src/nytimes/nytimes.service';
import { GuardiansService } from '../guardians/guardians.service';


@Injectable()
export class Ja3piService {
    private keys: { nytimes: string; guardians: string };

    constructor(
        private readonly configService: ConfigService,
        private readonly nytimesService: NYTimesService,
        private readonly guardiansService: GuardiansService,
    ) {
        this.keys = {
            nytimes: this.configService.get<string>('nytimes.key'),
            guardians: this.configService.get<string>('guardians.key'),
        };
    }

    search(data: SearchDTO): Observable<SearchResponse[]> {
        return zip(this.guardiansSearch(data), this.nytimesSearch(data)).pipe(
            map(response => response[0].concat(response[1]))
        );
    }

    guardiansSearch(data: SearchDTO): Observable<SearchResponse[]> {
        const queryData = new GuardiansQuery({
            q: data.q,
            beingDate: data.from_date,
            endDate: data.to_date,
            page: data.page
        });
        return this.guardiansService.search(queryData, this.keys.guardians).pipe(
            map(response => response.data.response.results),
            map(results =>
                results.map<SearchResponse>(item => {
                    return {
                        source: 'The Guardian',
                        tittle: item.webTitle,
                        publicationDate: item.webPublicationDate,
                        section: item.sectionName,
                        type: item.type,
                        ulr: item.webUrl,                    
                    };
                }),
            ),
        );
    }

    nytimesSearch(data: SearchDTO): Observable<SearchResponse[]> {
        const queryData = new NYTimesQuery({
            q: data.q,
            beingDate: data.from_date,
            endDate: data.to_date,
            page: data.page,
        });
        return this.nytimesService.search(queryData, this.keys.nytimes).pipe(
            map(response => response.data.response.docs),
            map(docs =>
                docs.map<SearchResponse>(item => {
                    return {
                        source: 'The New York Times',
                        tittle: item.abstract,
                        publicationDate: item.pub_date,
                        section: item.section_name,
                        type: item.document_type,
                        ulr: item.web_url,
                    };
                }),
            ),
        );
    }
}
