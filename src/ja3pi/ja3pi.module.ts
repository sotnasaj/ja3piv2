import { Module, HttpModule } from '@nestjs/common';
//Services
import { Ja3piService } from './ja3pi.service';
//Modules
import { NYTimesModule } from '../nytimes/nytimes.module';
import { GuardiansModule } from '../guardians/guardians.module';
import { AuthModule } from '../auth/auth.module';
//Controllers
import { Ja3piController } from './ja3pi.controller';

@Module({
    imports: [AuthModule, HttpModule, NYTimesModule, GuardiansModule],
    controllers: [Ja3piController],
    providers: [Ja3piService]
})
export class Ja3piModule { }
