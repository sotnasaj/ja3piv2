import {
    Controller, Req, Get, Query, UsePipes,
    ValidationPipe, UseGuards, UnauthorizedException
} from '@nestjs/common';
import { Request } from 'express';
//Services
import { Ja3piService } from './ja3pi.service';
// interfaces and DTO's
import { SearchDTO } from './dto/SearchDTO';
//Guards
import { JwtAuthGuard } from '../auth/auth.guard';
import { IUser } from 'src/auth/interfaces/user';

@UseGuards(JwtAuthGuard)
@Controller('ja3pi')
export class Ja3piController {
    constructor(private readonly ja3piService: Ja3piService) { }

    @Get('search')
    @UsePipes(new ValidationPipe({ transform: true }))
    searchNYTimes(@Query() searchQuery: SearchDTO, @Req() request: Request) {

        const user = request.user as IUser;
        if (searchQuery.source === 'nytimes')
            return this.ja3piService.nytimesSearch(searchQuery);

        if (user.username === 'GUEST') throw new UnauthorizedException();

        if (searchQuery.source === 'guardians')
            return this.ja3piService.guardiansSearch(searchQuery);

        return this.ja3piService.search(searchQuery);
    }
}
