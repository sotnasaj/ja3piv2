import { Test, TestingModule } from '@nestjs/testing';
import { Ja3piController } from './ja3pi.controller';

describe('Ja3pi Controller', () => {
  let controller: Ja3piController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [Ja3piController],
    }).compile();

    controller = module.get<Ja3piController>(Ja3piController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
