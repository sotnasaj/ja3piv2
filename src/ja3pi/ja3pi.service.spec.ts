import { Test, TestingModule } from '@nestjs/testing';
import { Ja3piService } from './ja3pi.service';

describe('Ja3piService', () => {
  let service: Ja3piService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [Ja3piService],
    }).compile();

    service = module.get<Ja3piService>(Ja3piService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
