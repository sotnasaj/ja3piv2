import { Injectable, HttpService } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AxiosResponse } from 'axios';
//Services
import { ConfigService } from '@nestjs/config';
//Interfaces and DTO's
import { NYTimesQuery } from './dto/nytimesQuery';
import { NYTimesResponse } from './interfaces/nytimesResponse';

@Injectable()
export class NYTimesService {
    private readonly endpoint: string;
    private readonly fields: string;

    constructor(
        private readonly configService: ConfigService,
        private readonly httpService: HttpService,
    ) {
        this.endpoint = configService.get<string>('nytimes.endpoint');
    }

    search(queryData: NYTimesQuery, apiKey: string): Observable<AxiosResponse<NYTimesResponse>> {
        return this.httpService.get<NYTimesResponse>(
            this.endpoint + queryData.toUri() + `&api-key=${apiKey}`
        )
    }
}