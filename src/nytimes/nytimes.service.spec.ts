import { Test, TestingModule } from '@nestjs/testing';
import { NYTimesService } from './nytimes.service';

describe('NytimesService', () => {
  let service: NYTimesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NYTimesService],
    }).compile();

    service = module.get<NYTimesService>(NYTimesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
