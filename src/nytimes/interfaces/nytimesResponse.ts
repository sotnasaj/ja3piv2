export interface NYTimesResponse {
    status: string,
    copyright: string,
    response: {
        docs: NYTimesDocuments[]
    }
}
interface NYTimesDocuments {
    abstract: string,
    web_url: string,
    pub_date: string,
    document_type: string,
    section_name: string
}

