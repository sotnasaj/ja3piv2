export class NYTimesQuery {

    public q: string;
    public beingDate: string | undefined;
    public endDate: string | undefined;
    public page: number | undefined;
    public fields = ['web_url', 'abstract', 'section_name', 'pub_date', 'document_type'];


    constructor({ q, beingDate, endDate, page }: {
        q: string, beingDate?: string, endDate?: string, page?: number
    }
    ) {
        this.q = q;
        this.beingDate = beingDate;
        this.endDate = endDate;
        this.page = page
    }

    toUri() {
        const uri: string[] = [];
        uri.push(`?q=${this.q}`);
        if (this.beingDate) uri.push(`&being_date=${this.beingDate.replace(/-/g, '')}`);
        if (this.endDate) uri.push(`&end_date=${this.endDate.replace(/-/g, '')}`);
        if (this.page) uri.push(`&page=${this.page}`)
        uri.push(`&fl=${this.fields.join()}`);
        return uri.join('');
    }
}